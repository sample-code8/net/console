﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog;
using NLog.Extensions.Logging;

namespace NLogSampleApp
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var logger = LogManager.GetCurrentClassLogger();
            try
            {
                var config = new ConfigurationBuilder()
                   .SetBasePath(System.IO.Directory.GetCurrentDirectory()) //NuGet パッケージから `Microsoft.Extensions.Configuration.Json`
                   .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                   .Build();

                using var servicesProvider = new ServiceCollection()
                    .AddTransient<Runner>() // Runnerはカスタムクラスです
                    .AddLogging(loggingBuilder =>
                    {
                        // NLog を使用したロギング設定
                        loggingBuilder.ClearProviders();
                        loggingBuilder.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace);
                        loggingBuilder.AddNLog(config);
                    }).BuildServiceProvider();

                var runner = servicesProvider.GetRequiredService<Runner>();
                runner.DoAction("Action1");

                Console.WriteLine("Press ANY key to exit");
                Console.ReadKey();
            }
            catch (Exception ex)
            {
                // NLog: 例外をキャッチしてログに記録します。
                logger.Error(ex, "Stopped program because of exception");
                throw;
            }
            finally
            {
                // アプリケーションが終了する前に、内部タイマー/スレッドをフラッシュして停止するようにします (Linux でのセグメンテーション違反を回避します)。
                LogManager.Shutdown();
            }
        }
    }
}