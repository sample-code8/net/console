# .Netのコンソールサンプル集

`.Net`のコンソールサンプル集です。

## NLogサンプル

### サンプルソースの場所

\src\NLogSampleApp

### チュートリアル

1. コンソールプロジェクト作成
2. `NuGet`より下記パッケージを追加
   - [NLog.Extensions.Logging](https://www.nuget.org/packages/NLog.Extensions.Logging)
   - [Microsoft.Extensions.DependencyInjection](https://www.nuget.org/packages/Microsoft.Extensions.DependencyInjection)または他のDIライブラリ
   - [Microsoft.Extensions.Configuration.Json](https://www.nuget.org/packages/Microsoft.Extensions.Configuration.Json)
3. `nlog.config`ファイルの作成
    プロジェクトのルートに`nlog.config`(すべて小文字) ファイルを作成する。
    ※ ファイルプロパティの`出力ディレクトリにコピー`を"常にコピー"にすること。
    ※ 詳細は下記を参照。
    - [Configuration file](https://github.com/NLog/NLog/wiki/Configuration-file)
    - [Configuration options](https://nlog-project.org/config/)
4. プログラムの更新
   1. ランナークラスの作成
   2. `Dependency Injection`を利用したMicrosoftログの設定

### 参考URL

[Getting started with .NET Core 2 Console application](https://github.com/NLog/NLog/wiki/Getting-started-with-.NET-Core-2---Console-application)
